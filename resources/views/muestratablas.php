<!DOCTYPE html>
<html>
<head>
	<title>Tablas de multiplicar</title>
</head>
<body>
	<h1>Resultado</h1>
	<hr>
	<?php
		function muestratablas($num_tabla, $num_filas, $num_columnas, $inicio_tabla){
			$tab = "\t\t\t\t\t\t\t ";
			echo "Tabla del $num_tabla <br>";
			echo "mostrando ".($num_filas * $num_columnas)." <br>";
			echo "inicio de tabla $inicio_tabla <br>";
			$num_filas = (int) $num_filas;
			$num_tabla = (int) $num_tabla;
			$inicio_tabla = (int) $inicio_tabla;
			$num_columnas = (int) $num_columnas;
			$total = $num_filas*$num_columnas+$inicio_tabla;
			echo "Termina en ".$total." <br> ";
			for ($i=$inicio_tabla; $i < $total; $i++) { 
				if(($i-$inicio_tabla)%$num_filas==0){
					echo "<hr>";
					//echo $i*$num_tabla;
				}
				echo $i*$num_tabla;
				echo $tab;
			}
			//echo "Hola";
		}

		muestratablas($_GET['num_tabla'],$_GET['num_filas'],$_GET['num_columnas'],$_GET['num_pos_ini']);
	?>
</body>
</html>