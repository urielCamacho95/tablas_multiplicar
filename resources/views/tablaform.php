<!DOCTYPE html>
<html>
<head>
	<title>Tablas</title>
</head>
<body>
	<form action="<?php echo route('tablas') ?>" method="GET">
		<h1>Tablas de multiplicar</h1>
		<section>
			<h5>Introduce el numero de la tabla</h3>
			<input type="text" name="num_tabla">
			<h5>Introduce el numero de filas</h3>
			<input type="text" name="num_filas">
			<h5>Introduce el numero de columnas</h3>
			<input type="text" name="num_columnas">
			<h5>Introduce el numero de inicio</h3>
			<input type="text" name="num_pos_ini">
			<button type="submit">Enviar</button>
		</section>
	</form>

</body>
</html>